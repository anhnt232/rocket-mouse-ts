import Phaser from 'phaser'

import TextureKeys from '../consts/TextureKeys'
import SceneKeys from '../consts/SceneKeys'
import AnimationKeys from '../consts/AnimationKeys'

export default class Preloader extends Phaser.Scene
{
	constructor()
	{
		super(SceneKeys.Preloader)
	}

	preload()
	{
		this.load.image(TextureKeys.Background, 'assets/house/bg_repeat_340x640.png')
		this.load.image(TextureKeys.MouseHole, 'assets/house/object_mousehole.png')
		this.load.image(TextureKeys.Window1, 'assets/house/object_window1.png')
		this.load.image(TextureKeys.Window2, 'assets/house/object_window2.png')

		this.load.image(TextureKeys.Bookcase1, 'assets/house/object_bookcase1.png')
		this.load.image(TextureKeys.Bookcase2, 'assets/house/object_bookcase2.png')

		this.load.image(TextureKeys.LaserEnd, 'assets/house/object_laser_end.png')
		this.load.image(TextureKeys.LaserMiddle, 'assets/house/object_laser.png')

		this.load.image(TextureKeys.Coin, 'assets/house/object_coin.png')
		this.load.atlas(TextureKeys.RocketMouse, 'assets/characters/rocket-mouse.png', 
		'assets/characters/rocket-mouse.json')
	}

	create()
	{
		this.scene.start(SceneKeys.Game)
	}
}
