# Infinite Runner

```bash
npm install parcel
npm install
npm run start
```

## Credits

[House Background art](https://www.gameartguppy.com/shop/house-1-repeatable-background/) from Game Art Guppy

[Rocket Mouse art](https://www.gameartguppy.com/shop/rocket-mouse-game-art-character/) from Game Art Guppy
